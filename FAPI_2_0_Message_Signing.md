# FAPI 2.0 Message Signing

This is the old location of the FAPI 2.0 Message Signing Profile.

The latest HTML version of the document can be found here: https://openid.bitbucket.io/fapi/fapi-2_0-message-signing.html

The source on bitbucket can be found here: https://bitbucket.org/openid/fapi/src/master/fapi-2_0-message-signing.md

