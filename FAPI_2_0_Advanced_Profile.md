# FAPI 2.0 Message Signing

The FAPI 2.0 Message Signing Profile was previously known as the FAPI 2.0 Advanced Profile. 

The latest HTML version of the document can be found here: https://openid.bitbucket.io/fapi/fapi-2_0-message-signing.html

The source on bitbucket can be found here: https://bitbucket.org/openid/fapi/src/master/FAPI_2_0_Message_Signing.md

